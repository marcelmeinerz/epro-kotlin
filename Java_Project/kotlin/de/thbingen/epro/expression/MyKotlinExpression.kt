package de.thbingen.epro.expression

class MyKotlinExpression {

	fun foo(x:Any){
		when (x) {
			is Number -> print("isNumber")
			0, 1 -> print("x == 0 or x == 1")
			in 1..10 -> print("x is in the range")
			!in 10..20 -> print("x is outside the range")
			else -> print("none of the above")
		}
	}
}