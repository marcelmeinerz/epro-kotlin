package de.thbingen.epro.methods

class MyKotlinMethods {
	//Main
	fun main(args : Array<String>){
		val sum = sum(1,2)
//		val sum = sum()
//		val sum = sum(y = 4)
//		val sum = sum(y=4,x=5)
//		val sum2 = sum2(1,2)
//		val zSquare = square(z(1,2))
//		val zSquare2 = sum(1,2).squared()
	}
	
	
	//Sum function
	fun sum(x : Int , y : Int) : Int{
		return x+y
	}
	
//	fun sum(x : Int , y : Int = 4) : Int{
//		return x+y
//	}
//	
//	fun sum(x : Int=2 , y : Int = 4) : Int{
//		return x+y
//	}
	
	//Sum inline function
	fun sum2(x : Int , y : Int) = x + y
	
	//High Order function
	
	val z = fun (x : Int , y : Int) : Int{
		return x+y
	}
	
	fun square (x : Int) : Int{
		return x*x
	}
	
	//Extension Functions
	fun Int.squared(): Int {
        return this * this
	}
	
}