package de.thbingen.epro.operator

data class MyDayOperators(val day: String) {
	
	val dayMap = hashMapOf("Sonntag" to 0,"Montag" to 1,"Dienstag" to 2,"Mittwoch" to 3,"Donnerstag" to 4,"Freitag" to 5,"Samstag" to 6 )
	val keyMap = hashMapOf(0 to "Sonntag",1 to "Montag",2 to "Dienstag",3 to "Mittwoch",4 to "Donnerstag",5 to "Freitag",6 to "Samstag" )
	
	operator fun plus(increment: MyDayOperators): MyDayOperators{
		val incr:Int = dayMap[increment.day] ?: 0
		val current:Int = dayMap[day] ?: 0
		var returnDay:Int = (incr+current)%dayMap.size
		
		return MyDayOperators(keyMap[returnDay] ?: "Nicht mein Tag")
	}
	
}