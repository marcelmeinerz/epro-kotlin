package de.thbingen.epro.nullable

class MyNullableKotlin {
	
	fun main(args :Array<String>){
		
		var a: String = "hello"
		//a = null // Compiler-Fehler!

		var b: String? = "world"
		b = null // ok
		
		
		val length: Int? = b?.length
		
		val nullExpression: Int? = b?.length ?: throw IllegalStateException("b is missing!")
		
		val length2: Int = b!!.length
	
	}
	
}