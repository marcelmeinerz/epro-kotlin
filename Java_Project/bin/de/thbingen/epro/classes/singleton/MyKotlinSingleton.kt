package de.thbingen.epro.classes.singleton

object MyKotlinSingleton {
	fun print() {
		println("MyKotlinSingleton")
	}
}