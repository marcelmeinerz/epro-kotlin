package de.thbingen.epro.classes.singelton;

public class MyJavaSingelton {
	
	private static MyJavaSingelton INSTANCE = new MyJavaSingelton();
	
	private MyJavaSingelton(){}
	
	public static MyJavaSingelton getInstance () {
		
		return INSTANCE;
		
	}
	public String getString(){
		
		return "Ich bin ein java Singleton";
		
	}
}
